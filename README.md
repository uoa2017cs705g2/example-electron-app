This is an example Electron app customized by Ryan to show the basic idea of what we need.

There is a System Tray icon, when clicked it opens a small HTML view for quick access to status information, etc.

There is a button that will open a full window.

The icon is hidden from the task bar.

All of this can be customized, this is just an example for reference on some basics of how it works.

To Run: `electron /path/to/folder`

You will first have to install the electron package from NPM. `npm install electron -g` should do it? If it doesnt you will find it on google. Kek